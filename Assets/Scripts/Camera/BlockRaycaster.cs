using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// raycasts for blocks while right mouse button held down
// only if left mouse button not held down

public class BlockRaycaster : MonoBehaviour
{
    [SerializeField] private LayerMask raycastLayers;    // for 'activating' blocks

    private float raycastRadius = 0.025f;
    private float raycastDistance = 1000f;

    private Camera mainCam;

    private void Start()
    {
        // get Camera.main once only for efficiency
        mainCam = Camera.main;
    }

    private void Update()
    {
        if (Input.GetMouseButton(0))                        // can't raycast while moving camera around
        {
            GameEvents.OnRaycastBlock?.Invoke(null);        // deselect all blocks
            return;
        }

        if (Input.GetMouseButton(1))                        // right mouse
            DoRaycast(Input.mousePosition);
        else if (Input.GetMouseButtonUp(1))
            GameEvents.OnRaycastBlock?.Invoke(null);        // deselect all blocks / BlockUI
    }


    // raycast for target collider layer from mouse/touch screenPosition
    private void DoRaycast(Vector2 mousePosition)
    {
        Ray ray = mainCam.ScreenPointToRay(mousePosition);

        // raycast to detect what was 'touched'
        //Physics.Raycast(ray, out RaycastHit hitInfo, raycastDistance, raycastLayers);
        Physics.SphereCast(ray, raycastRadius, out RaycastHit hitInfo, raycastDistance, raycastLayers);

        if (hitInfo.transform != null)
            GameEvents.OnRaycastBlock?.Invoke(hitInfo.transform.GetComponent<StackBlock>());
        else
            GameEvents.OnRaycastBlock?.Invoke(null);        // no blocks hilighted
    }
}