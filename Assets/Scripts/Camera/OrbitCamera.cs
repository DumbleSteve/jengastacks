using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


// enables Cinemachine camera orbiting of a specific stack or the table
// only when left mouse is held down

[RequireComponent(typeof(CinemachineFreeLook))]

public class OrbitCamera : MonoBehaviour
{
    [SerializeField] private Transform Table;

    private CinemachineFreeLook orbitCamera;

    private string XAxisName = "Mouse X";
    private string YAxisName = "Mouse Y";


    private void Awake()
    {
        orbitCamera = GetComponent<CinemachineFreeLook>();

        OnCameraOrbitTable();

        // disable orbiting (until left mouse is held down)
        orbitCamera.m_XAxis.m_InputAxisName = "";
        orbitCamera.m_YAxis.m_InputAxisName = "";
    }

    private void OnEnable()
    {
        GameEvents.OnCameraOrbitStack += OnCameraOrbitStack;        // sets stack to look at / follow
        GameEvents.OnCameraOrbitTable += OnCameraOrbitTable;        // look at / follow table
    }

    private void OnDisable()
    {
        GameEvents.OnCameraOrbitStack -= OnCameraOrbitStack;
        GameEvents.OnCameraOrbitTable -= OnCameraOrbitTable;     
    }


    // only enable orbiting when left mouse button is held down
    private void Update()
    {
        if (Input.GetMouseButton(0))
        {
            orbitCamera.m_XAxis.m_InputAxisValue = Input.GetAxis(XAxisName);
            orbitCamera.m_YAxis.m_InputAxisValue = Input.GetAxis(YAxisName);
        }
        else
        {
            orbitCamera.m_XAxis.m_InputAxisValue = 0;
            orbitCamera.m_YAxis.m_InputAxisValue = 0;
        }
    }

    // set stack to orbit around (look at/follow)
    private void OnCameraOrbitStack(Stack stack)
    {
        if (stack == null)
            return;

        orbitCamera.LookAt = stack.FocalPoint;
        orbitCamera.Follow = stack.FocalPoint;
    }

    private void OnCameraOrbitTable()
    {
        if (Table == null)
            return;

        orbitCamera.LookAt = Table.transform;
        orbitCamera.Follow = Table.transform;
    }
}
