using System.Collections;
using System.Collections.Generic;


public class GameEvents
{
    // fetch / receive data from web request

    public delegate void OnFetchStackDataDelegate();    
    public static OnFetchStackDataDelegate OnFetchStackData;        // initiate web request to fetch block data

    public delegate void OnStackDataFetchedDelegate(List<BlockData> data);
    public static OnStackDataFetchedDelegate OnStackDataFetched;    // on completion of web request to fetch stack data

    // stack building

    public delegate void OnStackBuiltDelegate(Stack newStack);
    public static OnStackBuiltDelegate OnStackBuilt;                // on completion of building a stack

    public delegate void OnAllStacksBuiltDelegate(List<Stack> stacks);
    public static OnAllStacksBuiltDelegate OnAllStacksBuilt;        // on completion of all stacks

    public delegate void OnResetStacksDelegate();
    public static OnResetStacksDelegate OnResetStacks;              // rebuild all stacks

    public delegate void OnReloadDataDelegate();
    public static OnReloadDataDelegate OnReloadData;                // destroy blocks, re-fetch stack data and build all stacks

    public delegate void OnDestroyStacksDelegate();
    public static OnDestroyStacksDelegate OnDestroyStacks;          // ready for rebuild

    // block elimination

    public delegate void OnEnablePhysicsDelegate(bool enable);
    public static OnEnablePhysicsDelegate OnEnablePhysics;          // switch isKinematic for all block rigidbodies

    public delegate void OnEliminateMasteryDelegate(string mastery, Stack stack);
    public static OnEliminateMasteryDelegate OnEliminateMastery;    // eliminate all blocks representing mastery, specific stack or all if null

    // camera

    public delegate void OnCameraOrbitStackDelegate(Stack stack);
    public static OnCameraOrbitStackDelegate OnCameraOrbitStack;    // set the stack that the orbit camera is to look at

    public delegate void OnCameraOrbitTableDelegate();
    public static OnCameraOrbitTableDelegate OnCameraOrbitTable;    // return camera to look at table

    public delegate void OnRaycastBlockDelegate(StackBlock block);
    public static OnRaycastBlockDelegate OnRaycastBlock;            // raycast from mouse point on right-mouse (hits blocks)
}
