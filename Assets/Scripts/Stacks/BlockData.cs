
// data for eack Jenga block, deserialised from json fetched via web request

[System.Serializable]
public class BlockData
{
    public int id;
    public string subject;
    public string grade;
    public string mastery;
    public string domainid;
    public string domain;
    public string cluster;
    public string standardid;
    public string standarddescription;
}
