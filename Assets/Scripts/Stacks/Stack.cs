using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
//using System.Threading.Tasks;
using TMPro;

// stack represents a grade (eg. Grade 6, Grade 7)
// comprises unlimited StackLevels, each containing up to 3 StackBlocks

public class Stack : MonoBehaviour
{
    [SerializeField] private StackLevel stackLevelPrefab;
    private List<StackLevel> StackLevels = new();

    [SerializeField] private TextMeshPro gradeTextFront;
    [SerializeField] private TextMeshPro gradeTextBack;

    [SerializeField] private Light spotLight;

    public Transform FocalPoint;                        // for orbit camera to look at

    public string StackGrade { get; private set; }      // grade this stack represents
    private List<BlockData> gradeBlockData = new();     // ordered (domain, cluster, standard ID), ready to build levels

    private float EvenLevelRotation = 90f;              // even levels are rotated by 90 degrees

    private float LevelSpacingY = 0.1f;                 // vertical distance between levels in the stack (for level spawn position)
    private float currentLevelY = 0;

    private float levelDropHeight = 2f;                 // on level instantiation - drop onto stack with LeanTween (not physics)

    private float levelBuildInterval = 0.075f;           // delay between building levels (seconds)

    private void OnEnable()
    {
        GameEvents.OnCameraOrbitStack += OnCameraOrbitStack;        // sets stack to look at / follow
        GameEvents.OnCameraOrbitTable += OnCameraOrbitTable;        // look at / follow table

        GameEvents.OnResetStacks += OnResetStacks;
        //GameEvents.OnDestroyStacks += OnDestroyStacks;
        GameEvents.OnReloadData += OnReloadData;
    }

    private void OnDisable()
    {
        GameEvents.OnCameraOrbitStack -= OnCameraOrbitStack;
        GameEvents.OnCameraOrbitTable -= OnCameraOrbitTable;

        GameEvents.OnResetStacks -= OnResetStacks;
        //GameEvents.OnDestroyStacks -= OnDestroyStacks;
        GameEvents.OnReloadData -= OnReloadData;
    }

    // sort blocks by domain, cluster and standard ID
    // build all the levels in this stack, in groups of up to 3
    // delay between each level for visual effect
    public IEnumerator BuildLevels(string gradeLevel, List<BlockData> gradeData)       
    {
        // orbit camera watches stack being built
        //GameEvents.OnOrbitStack?.Invoke(this);

        gradeTextFront.enabled = false;
        gradeTextBack.enabled = false;

        // gradeData is not yet ordered as required for block build order
        gradeBlockData = gradeData?.OrderBy(x => x.domain).ThenBy(x => x.cluster).ThenBy(x => x.standardid).ToList();
        StackLevels = new();

        StackGrade = gradeTextFront.text = gradeTextBack.text = gradeLevel;
        currentLevelY = transform.position.y;

        List<BlockData> currentLevelBlocks = new();        // list of blocks (max 3) for each level

        // iterate through all blocks in this stack, grouping into levels of (up to) 3 blocks each
        foreach (var block in gradeBlockData)
        {
            currentLevelBlocks.Add(block);

            if (currentLevelBlocks.Count == StackBuilder.MaxBlocksPerLevel)     // new group of 3 blocks in level
            {
                yield return StartCoroutine(BuildLevelBlocks(currentLevelBlocks));
                currentLevelBlocks.Clear();                     // reset for next group of blocks
            }
        }

        // build a final level for any remaining (ie. 1 or 2) blocks
        if (currentLevelBlocks.Count > 0)
        {
            yield return StartCoroutine(BuildLevelBlocks(currentLevelBlocks));
        }

        gradeTextFront.enabled = true;
        gradeTextBack.enabled = true;
    }

    // build a level (1-3 blocks)
    // delay before build for visual effect
    private IEnumerator BuildLevelBlocks(List<BlockData> levelBlocks)
    {
        // new stack level, child of this stack
        var newLevel = Instantiate(stackLevelPrefab, transform);       
        newLevel.name = $"Level {StackLevels.Count}";

        StackLevels.Add(newLevel);

        // offset level position
        newLevel.transform.position = new Vector3(transform.position.x, currentLevelY + levelDropHeight, transform.position.z);
        currentLevelY += LevelSpacingY;

        yield return new WaitForSeconds(levelBuildInterval);
        newLevel.BuildBlocks(levelBlocks, this);

        // even numbered levels are rotated 90 degrees (after child blocks built)
        bool evenLevel = StackLevels.Count % 2 == 0;
        if (evenLevel)
            newLevel.transform.rotation = Quaternion.Euler(0, EvenLevelRotation, 0);

        // drop with LeanTween (not physics, as activating physics causes stack to wobble etc)
        newLevel.Drop(currentLevelY);
    }


    private void OnCameraOrbitStack(Stack stack)
    {
        spotLight.enabled = stack == this;
    }

    private void OnCameraOrbitTable()
    {
        spotLight.enabled = false;
    }

    private void OnResetStacks()
    {
        StopAllCoroutines();
    }

    private void OnReloadData()
    {
        StopAllCoroutines();
    }

    public void DestroyLevels()
    {
        StopAllCoroutines();

        foreach (var level in StackLevels)
        {
            level.DestroyBlocks();
            Destroy(level.gameObject);
        }

        StackLevels.Clear();
    }
}
