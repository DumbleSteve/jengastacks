using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using TMPro;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Renderer))]
[RequireComponent(typeof(Renderer))]
[RequireComponent(typeof(AudioSource))]

// a single block in a StackLevel within a Stack
// encapsulates a BlockData struct, set according to data from web request
// built from prefab according to mastery (glass/wood/stone)
// handles its own elimination / destruction and plays audio/particle effects

public class StackBlock : MonoBehaviour
{
    [SerializeField] private ParticleSystem eliminateParticles;
    [SerializeField] private AudioClip explosionAudio;
    [SerializeField] private AudioClip smashAudio;

    [SerializeField] private AudioSource collisionSource;        // to allow pitch adjust for different materials
    [SerializeField] private AudioClip collisionAudio;

    [SerializeField] private TextMeshPro label1;                // mastery, front
    [SerializeField] private TextMeshPro label2;                // mastery, back

    private float eliminatePhysicsDelay = 0.5f;                 // pause between elimination and enabling of physics, for effect

    public BlockData BlockData { get; private set; }

    private Stack parentStack;

    private Rigidbody blockRigidbody;
    private Collider blockCollider;
    private Renderer blockRenderer;

    private Outline outline;


    private void Awake()
    {
        blockRigidbody = GetComponent<Rigidbody>();
        blockCollider = GetComponent<Collider>();
        blockRenderer = GetComponent<Renderer>();
        blockRenderer = GetComponent<Renderer>();

        outline = GetComponent<Outline>();

        collisionSource = GetComponent<AudioSource>();
        collisionSource.clip = collisionAudio;
    }

    private void OnEnable()
    {
        GameEvents.OnEnablePhysics += OnEnablePhysics;
        GameEvents.OnEliminateMastery += OnEliminateMastery;
        GameEvents.OnRaycastBlock += OnRaycastBlock;            // outline
    }

    private void OnDisable()
    {
        GameEvents.OnEnablePhysics -= OnEnablePhysics;
        GameEvents.OnEliminateMastery -= OnEliminateMastery;
        GameEvents.OnRaycastBlock -= OnRaycastBlock;
    }

    private void OnRaycastBlock(StackBlock block)
    {
        outline.enabled = block == this;
    }

    // 'destroy' this block if it matches the mastery and belongs to the stack (if specified)
    // pause between elimination and activation of physics for visual effect
    private async void OnEliminateMastery(string mastery, Stack stack)
    {
        if (stack != null && stack != parentStack)
            return;

        if (BlockData.mastery == mastery)
            Eliminate();

        await Task.Delay((int)(eliminatePhysicsDelay * 1000f));

        OnEnablePhysics(true);      // all blocks!!
    }

    private void Eliminate()
    {
        if (! blockCollider.enabled)     // already eliminated
            return;

        blockCollider.enabled = false;
        blockRenderer.enabled = false;

        if (eliminateParticles != null)
            eliminateParticles.Play();

        if (explosionAudio != null)
            AudioSource.PlayClipAtPoint(explosionAudio, transform.position);

        if (smashAudio != null)
            AudioSource.PlayClipAtPoint(smashAudio, transform.position);
    }

    private void OnEnablePhysics(bool enable)
    {
        blockRigidbody.isKinematic = !enable;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collisionAudio != null)
        {
            if (collision.gameObject.CompareTag("Table") || collision.gameObject.CompareTag("Floor"))
                collisionSource.Play();
        }
    }

    // associate block with data fetched from web request
    public void SetData(BlockData block, Stack parent)
    {
        BlockData = block;
        parentStack = parent;

        label1.text = label2.text = BlockLabel();
    }

    private string BlockLabel()
    {
        if (BlockData == null)
            return "???";

        switch (BlockData.mastery)
        {
            case "0":
            default:
                return "";

            case "1":
                return "Learned";

            case "2":
                return "Mastered";
        }
    }
}
