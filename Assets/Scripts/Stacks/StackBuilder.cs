using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
//using System.Threading.Tasks;

// builds stacks of blocks from data fetched from a web request
// fetched data has been deserialised into a List<BlockData>, but is not yet sorted
// as required for the order in which blocks in the stack are to be built

public class StackBuilder : MonoBehaviour
{
    public static int StacksToBuild = 3;
    public static int MaxBlocksPerLevel = 3;

    private List<BlockData> orderedBlockData;      // ordered by grade, for grouping into stacks

    private Dictionary<string, List<BlockData>> gradeBlockDict = new();     // key is grade

    [SerializeField] private Transform firstStackPosition;
    [SerializeField] private Stack stackPrefab;
    private List<Stack> stacks;

    private float stackSpacingX = 1.8f;        // horizontal distance between stacks on the table
    private float currentStackX = 0;

    private float stackOrbitPause = 0.5f;       // pause before switching to new stack


    private void OnEnable()
    {
        GameEvents.OnStackDataFetched += OnStackDataFetched;
        GameEvents.OnResetStacks += OnResetStacks;
        GameEvents.OnDestroyStacks += OnDestroyStacks;
        GameEvents.OnReloadData += OnReloadData;
    }

    private void OnDisable()
    {
        GameEvents.OnStackDataFetched -= OnStackDataFetched;
        GameEvents.OnResetStacks -= OnResetStacks;
        GameEvents.OnDestroyStacks -= OnDestroyStacks;
        GameEvents.OnReloadData -= OnReloadData;
    }

    private void Start()
    {
        // initiate web request to fetch stack data
        GameEvents.OnFetchStackData?.Invoke();          // this is listening for OnStackDataFetched callback event
    }

    // web request has returned json results, which have been deserialized into a list
    // sort by grade in order to construct a dictionary, keyed by grade
    private void OnStackDataFetched(List<BlockData> blocks)
    {
        if (blocks == null || blocks.Count == 0)       // no data fetched!
            return;

        // orderedBlockData is ordered by grade, as each grade becomes its own stack
        orderedBlockData = blocks?.OrderBy(x => x.grade).ToList();   // only one linq call for efficiency

        gradeBlockDict = new();         // dictionary, keyed on grade
        stacks = new();

        // organise into dictionary, keyed by grade, value is unsorted BlockData list (will be sorted by each stack (grade))
        BuildDictionary();         // only one linq call for efficiency

        // build a stack for each grade, using each key in the dictionary
        StartCoroutine(BuildGradeStacks());
    }

    // orderedBlockData is ordered by grade only
    private void BuildDictionary()
    {
        string gradeLevel = "";         // new stack for each different grade level

        // organise into dictionary, keyed by grade, value is unsorted BlockData list
        foreach (var block in orderedBlockData)
        {
            if (block.grade != gradeLevel)
            {
                gradeLevel = block.grade;

                var gradeData = orderedBlockData.Where(x => x.grade == gradeLevel).ToList();

                gradeBlockDict.Add(gradeLevel, gradeData);
            }
        }
    }

    // build a Stack for each grade in the dictionary
    // build each Stack in turn, then each StackLevel in turn
    private IEnumerator BuildGradeStacks()
    {
        currentStackX = firstStackPosition.position.x;

        foreach (var grade in gradeBlockDict.Keys)
        {
            yield return StartCoroutine(BuildStack(grade));

            // don't exeed stack limit (won't fit on table!)
            if (stacks.Count == StacksToBuild)
                break;
        }

        yield return new WaitForSeconds(stackOrbitPause);

        //// orbit camera returns to first stack
        //GameEvents.OnCameraOrbitStack?.Invoke(stacks[0]);

        GameEvents.OnAllStacksBuilt?.Invoke(stacks);
    }

    private IEnumerator BuildStack(string grade)
    {
        // new stack, child of this builder
        Stack newStack = Instantiate(stackPrefab, transform);       
        newStack.name = grade;

        newStack.transform.position = new Vector3(currentStackX, firstStackPosition.position.y, firstStackPosition.position.z);
        currentStackX += stackSpacingX;

        stacks.Add(newStack);

        // build all levels for this grade's data
        // sort blocks by domain, cluster and standard ID
        List<BlockData> gradeBlockData = gradeBlockDict[grade];
        yield return StartCoroutine(newStack.BuildLevels(grade, gradeBlockData));       // gradeBlocks grouped in 3's

        GameEvents.OnStackBuilt?.Invoke(newStack);
    }


    private void OnResetStacks()
    {
        // destroy all stacks (and therefore levels and blocks)
        GameEvents.OnDestroyStacks?.Invoke();

        // rebuild a stack for each grade, from the dictionary
        StartCoroutine(BuildGradeStacks());
    }

    private void OnReloadData()
    {
        // destroy all stacks (and therefore levels and blocks)
        GameEvents.OnDestroyStacks?.Invoke();

        // initiate web request to fetch stack data
        GameEvents.OnFetchStackData?.Invoke();          // this is listening for OnStackDataFetched callback event
    }


    private void OnDestroyStacks()
    {
        StopAllCoroutines();

        // destroy all stacks (and therefore levels and blocks)
        foreach (var stack in stacks)
        {
            stack.DestroyLevels();

            Destroy(stack.gameObject);
        }

        stacks.Clear();
    }
}
