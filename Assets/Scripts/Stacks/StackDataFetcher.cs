using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.Networking;


// get json data from web API (at dataUri)
// and deserialise into a list for BlockData structs

public class StackDataFetcher : MonoBehaviour
{
    [SerializeField] private string dataUri;        // as provided, for student grade data (json)


    private void OnEnable()
    {
        GameEvents.OnFetchStackData += OnFetchStackData;
    }

    private void OnDisable()
    {
        GameEvents.OnFetchStackData -= OnFetchStackData;
    }


    private void OnFetchStackData()
    {
        StartCoroutine(GetRequest(dataUri));        // waits for request to complete
    }

    private IEnumerator GetRequest(string uri)
    {
        using UnityWebRequest request = UnityWebRequest.Get(uri);

        // wait for web request to complete
        yield return request.SendWebRequest();

        // web request has completed
        switch (request.result)
        {
            case UnityWebRequest.Result.InProgress:
                //GameEvents.OnStackDataFetchProgress?.Invoke(request.downloadProgress);
                break;

            // deserialise json result into a BlockData list
            case UnityWebRequest.Result.Success:
                List<BlockData> blockData = JsonConvert.DeserializeObject<List<BlockData>>(request.downloadHandler.text);
                // broadcast deserialised data
                GameEvents.OnStackDataFetched?.Invoke(blockData);
                break;

            case UnityWebRequest.Result.ConnectionError:
            case UnityWebRequest.Result.ProtocolError:
            case UnityWebRequest.Result.DataProcessingError:
                Debug.LogError($"GetRequest() returned error '{request.error}");
                break;
        }
    }
}
