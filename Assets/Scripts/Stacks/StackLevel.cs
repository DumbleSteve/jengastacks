using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// a level in a stack, contains max 3 StackBlocks arranged within a square

public class StackLevel : MonoBehaviour
{
    [SerializeField] private StackBlock stoneBlockPrefab;
    [SerializeField] private StackBlock woodBlockPrefab;
    [SerializeField] private StackBlock glassBlockPrefab;

    [SerializeField] private AudioClip levelDropAudio;

    private List<StackBlock> LevelBlocks = new();

    private Stack parentStack;

    private float levelDropTime = 0.2f;

    // build blocks from levelBlocks (max 3)
    public void BuildBlocks(List<BlockData> levelBlocks, Stack parent)
    {
        parentStack = parent;
        LevelBlocks.Clear();

        int blockCounter = 0;

        foreach (BlockData block in levelBlocks)
        {
            StackBlock blockPrefab;     // set according to mastery

            switch (block.mastery)
            {
                case "0":
                default:
                    blockPrefab = glassBlockPrefab;
                    break;

                case "1":
                    blockPrefab = woodBlockPrefab;
                    break;

                case "2":
                    blockPrefab = stoneBlockPrefab;
                    break;
            }

            // new block, child of this level
            StackBlock newBlock = Instantiate(blockPrefab, transform);       
            newBlock.name = $"Block {blockCounter}";
            newBlock.SetData(block, parentStack);

            // offset block position (first block in level is centered)
            // ensure that level is square with no overhanging blocks, for maximum stability
            // calculate block spacing accordingly from block width and length...
            var blockWidth = blockPrefab.transform.localScale.x;
            var blockLength = blockPrefab.transform.localScale.z;

            float spaceToFill = blockLength - (blockWidth * StackBuilder.MaxBlocksPerLevel);
            float blockSpacing = blockWidth + (spaceToFill / 2f);

            if (blockCounter == 1)
                newBlock.transform.position = new Vector3(transform.position.x - blockSpacing, transform.position.y, transform.position.z);
            else if (blockCounter == 2)
                newBlock.transform.position = new Vector3(transform.position.x + blockSpacing, transform.position.y, transform.position.z);

            LevelBlocks.Add(newBlock);

            blockCounter++;
        }
    }

    // drop with LeanTween (not physics, as activating physics causes stack to wobble etc)
    public void Drop(float toPositionY)
    {
        LeanTween.moveY(gameObject, toPositionY, levelDropTime)
                .setEaseInQuint()
                .setOnComplete(() =>
                {
                    if (levelDropAudio != null)
                        AudioSource.PlayClipAtPoint(levelDropAudio, transform.position);
                });
    }

    public void DestroyBlocks()
    {
        foreach (var block in LevelBlocks)
        {
            Destroy(block.gameObject);
        }

        LevelBlocks.Clear();
    }
}

