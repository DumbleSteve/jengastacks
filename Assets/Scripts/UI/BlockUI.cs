using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

// UI panel to show details of selected block,
// as per data fetched from web API

public class BlockUI : MonoBehaviour
{
    [SerializeField] private Image blockPanel;

    [SerializeField] private TextMeshProUGUI gradeText;
    [SerializeField] private TextMeshProUGUI subjectText;
    [SerializeField] private TextMeshProUGUI domainText;
    [SerializeField] private TextMeshProUGUI clusterText;
    [SerializeField] private TextMeshProUGUI standardIDText;
    [SerializeField] private TextMeshProUGUI stdDescriptionText;

    private Vector2 panelStartScale;
    private float panelScaleTime = 0.3f;
    private bool blockPanelVisible = false;

    private void Awake()
    {
        panelStartScale = blockPanel.transform.localScale;

        blockPanelVisible = false;
        blockPanel.transform.localScale = new Vector2(panelStartScale.x, 0f);
        blockPanel.gameObject.SetActive(true);
    }

    private void OnEnable()
    {
        GameEvents.OnRaycastBlock += OnRaycastBlock;
    }

    private void OnDisable()
    {
        GameEvents.OnRaycastBlock -= OnRaycastBlock;
    }

    // show block data for block hit by raycast
    private void OnRaycastBlock(StackBlock block)
    {
        ShowBlockPanel(block != null);

        if (block != null)
        {
            var blockData = block.BlockData;

            gradeText.text = blockData.grade;
            subjectText.text = blockData.subject;
            domainText.text = blockData.domain;
            clusterText.text = blockData.cluster;
            standardIDText.text = blockData.standardid;
            stdDescriptionText.text = blockData.standarddescription;
        }
    }

    // roll block panel up/down (pivot is top centre)
    private void ShowBlockPanel(bool show)
    {
        if (blockPanelVisible == show)       // no change, do nothing
                return;

        if (show)
        {
            blockPanel.transform.localScale = new Vector2(panelStartScale.x, 0f);
            blockPanelVisible = true;

            LeanTween.scaleY(blockPanel.gameObject, panelStartScale.y, panelScaleTime)
                        .setEaseOutBack();
                        //.setOnComplete(() => { blockPanelVisible = true; });
        }
        else    // hide
        {
            //blockPanel.transform.localScale = new Vector2(panelStartScale.x, 0f);
            //blockPanelVisible = false;

            LeanTween.scaleY(blockPanel.gameObject, 0f, panelScaleTime / 2f)
                        .setEaseInQuart()
                        .setOnComplete(() => { blockPanelVisible = false; });
        }
    }
}
