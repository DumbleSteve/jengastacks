using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


// toggle button representing a stack
// instantiated as each stack is built

[RequireComponent(typeof(Toggle))]

public class SelectStackButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI stackName;
    [SerializeField] private Color onColour;
    [SerializeField] private Color offColour;

    private Toggle button;
    private Stack buttonStack;          // stack that this button is linked to

    private void Awake()
    {
        button = GetComponent<Toggle>();
    }

    private void OnEnable()
    {
        button.onValueChanged.AddListener(SelectStack);        // camera orbits buttonStack

        GameEvents.OnAllStacksBuilt += OnAllStacksBuilt;        // first stack is selected once all built
    }

    private void OnDisable()
    {
        button.onValueChanged.RemoveListener(SelectStack);

        GameEvents.OnAllStacksBuilt -= OnAllStacksBuilt;
    }

    private void SelectStack(bool selected)
    {
        button.GetComponent<Image>().color = (button.isOn ? onColour : offColour);

        if (selected)
            GameEvents.OnCameraOrbitStack?.Invoke(buttonStack);
    }

    // first stack is selected once all built
    private void OnAllStacksBuilt(List<Stack> stacks)
    {
        button.interactable = true;
        //button.isOn = buttonStack == stacks[0];

        //if (buttonStack == stacks[0])
        //    SelectStack(true);
    }

    // initialise
    public void SetStack(Stack stack, ToggleGroup group)
    {
        buttonStack = stack;
        stackName.text = stack.StackGrade;

        button.group = group;               // toggle group
        button.interactable = false;        // until all stacks built
    }
}
