using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// handles all button interaction
// creates a toggle button for each stack as it is built

public class UIController : MonoBehaviour
{
    [SerializeField] private Button TestStackButton;        // eliminate glass blocks from stackInFocus

    [SerializeField] private Button TestAllStacksButton;    // eliminate glass blocks from all stacks
    [SerializeField] private Button ResetStacksButton;      // destroy and rebuild all stacks from current data
    [SerializeField] private Button ReloadDataButton;       // inititate new web request

    [SerializeField] private TextMeshProUGUI StatusText;

    [Header("Buttons to Select Stack")]
    [SerializeField] private ToggleGroup SelectStackPanel;
    [SerializeField] private SelectStackButton SelectStackButtonPrefab;


    private const string GlassMastery = "0";                // for block elimination

    private Stack stackInFocus = null;

    private void Awake()
    {
        EnableButtons(false);
    }

    private void OnEnable()
    {
        TestStackButton.onClick.AddListener(TestCurrentStack);
        TestAllStacksButton.onClick.AddListener(TestAllStacks);
        ResetStacksButton.onClick.AddListener(ResetStacks);
        ReloadDataButton.onClick.AddListener(ReloadData);

        GameEvents.OnFetchStackData += OnFetchStackData;
        GameEvents.OnStackDataFetched += OnStackDataFetched;

        GameEvents.OnCameraOrbitStack += OnCameraOrbitStack;
        GameEvents.OnCameraOrbitTable += OnCameraOrbitTable;    // look at / follow table

        GameEvents.OnStackBuilt += OnStackBuilt;                // add stack button
        GameEvents.OnAllStacksBuilt += OnAllStacksBuilt;
        GameEvents.OnDestroyStacks += OnDestroyStacks;          // clear stack buttons
    }

    private void OnDisable()
    {
        TestStackButton.onClick.RemoveListener(TestCurrentStack);
        TestAllStacksButton.onClick.RemoveListener(TestAllStacks);

        ResetStacksButton.onClick.RemoveListener(ResetStacks);
        ReloadDataButton.onClick.RemoveListener(ReloadData);

        GameEvents.OnFetchStackData -= OnFetchStackData;
        GameEvents.OnStackDataFetched -= OnStackDataFetched;

        GameEvents.OnCameraOrbitStack -= OnCameraOrbitStack;
        GameEvents.OnCameraOrbitTable -= OnCameraOrbitTable;

        GameEvents.OnStackBuilt -= OnStackBuilt;
        GameEvents.OnAllStacksBuilt -= OnAllStacksBuilt;
        GameEvents.OnDestroyStacks -= OnDestroyStacks;
    }

    // create a new button for the stack just built
    private void OnStackBuilt(Stack newStack)
    {
        SelectStackButton stackButton = Instantiate(SelectStackButtonPrefab, SelectStackPanel.transform);     // vertical layoout group
        stackButton.SetStack(newStack, SelectStackPanel);
    }

    // clear stack buttons
    private void OnDestroyStacks()
    {
        EnableButtons(false);

        foreach (Transform stackButton in SelectStackPanel.transform)
        {
            Destroy(stackButton.gameObject);
        }
    }

    private void OnAllStacksBuilt(List<Stack> stacks)
    {
        EnableButtons(true);
    }

    private void EnableButtons(bool enable)
    {
        TestStackButton.interactable = enable;
        TestAllStacksButton.interactable = enable;
        ResetStacksButton.interactable = enable;
        ReloadDataButton.interactable = enable;
    }

    private void OnCameraOrbitStack(Stack stack)
    {
        stackInFocus = stack;
        TestStackButton.interactable = stack != null;
    }

    private void OnCameraOrbitTable()
    {
        stackInFocus = null;
        TestStackButton.interactable = false;
    }

    private void TestCurrentStack()
    {
        if (stackInFocus != null)
            GameEvents.OnEliminateMastery?.Invoke(GlassMastery, stackInFocus);   // enable physics and eliminate glass blocks
    }

    private void TestAllStacks()
    {
        GameEvents.OnCameraOrbitTable?.Invoke();
        GameEvents.OnEliminateMastery?.Invoke(GlassMastery, null);
    }

    private void ResetStacks()
    {
        GameEvents.OnCameraOrbitTable?.Invoke();
        GameEvents.OnResetStacks?.Invoke();
    }

    private void ReloadData()
    {
        GameEvents.OnCameraOrbitTable?.Invoke();
        GameEvents.OnReloadData?.Invoke();
    }


    private void OnFetchStackData()
    {
        StatusText.text = "Retrieving Data for Stacks...";
        EnableButtons(false);
    }

    private void OnStackDataFetched(List<BlockData> data)
    {
        StatusText.text = "";
    }
}
